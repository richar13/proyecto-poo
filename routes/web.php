<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

auth::routes();

route::get(`/home`, `homecontroller@index`)->name(`home`);

route::group([`middleware` => [`auth`]], function() {
route::get(`/lista/productos`,[`as` => `lista.productos`, `uses` =>`productocontroller@inicioproducto`]);
route::get(`/crear/productos`,[`as` => `crear.productos`, `uses` =>`productocontroller@crearproducto`]);
route::get(`/guardar/productos`,[`as` => `guardar.productos`, `uses` =>`productocontroller@guardarproducto`]);
route::get(`/lista/clients`,[`as` => `lista.cliente`, `uses` =>`clientecontroller@iniciocliente`]);
route::get(`/crear/clientes`,[`as` => `crear.cliente`, `uses` =>`clientecontroller@crearcliente`]);
route::get(`/guardar/clientes`,[`as` => `guardar.cliente`, `uses` =>`clientecontroller@guardarcliente`]);
});